#Branching Schema
Having a branch for each member feels kind of overkill for a small group, so we've decided to go with just 2 branches.
A development branch and a production branch. We will all work on the production branch, and after each main feature, we
will push to project to the production branch.


- Carlos wrote this :D <--
- Juan wrote this :P
